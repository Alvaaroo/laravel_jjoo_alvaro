<?php

use Illuminate\Database\Seeder;
use App\Deporte;
use App\Competidor;

class DatabaseSeeder extends Seeder
{

	private $arrayDeportes = [
		[	"nombre" => "Atletismo",
			"pruebas" => 47,
			"numeroInicioCompetidores" => 100,
			"imagen" => "atletismo.png",
		],

		[	"nombre" => "Badminton",
			"pruebas" => 5,
			"numeroInicioCompetidores" => 500,
			"imagen" => "badminton.png",
		],

		[	"nombre" => "Ciclismo",
			"pruebas" => 22,
			"numeroInicioCompetidores" => 800,
			"imagen" => "ciclismo.png",
		],

		[	"nombre" => "Boxeo",
			"pruebas" => 13,
			"numeroInicioCompetidores" => 1000,
			"imagen" => "boxeo.png",
		],

		[	"nombre" => "Triatlón",
			"pruebas" => 2,
			"numeroInicioCompetidores" => 1200,
			"imagen" => "triatlon.png",
		],

		[	"nombre" => "Natación",
			"pruebas" => 34,
			"numeroInicioCompetidores" => 1500,
			"imagen" => "natacion.png",
		],
		
	];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competidores')->delete();
    	DB::table('deportes')->delete();
        $this->seedDeportes();
        $this->seedCompetidores();
    }


    public function seedDeportes()
    {
    	foreach ($this->arrayDeportes as $deporte) 
    	{
    		$d = new Deporte();
    		$d->nombre = $deporte['nombre'];
    		$d->slug = Str::slug($deporte['nombre']);
    		$d->pruebas = $deporte['pruebas'];
    		$d->numeroInicioCompetidores = $deporte['numeroInicioCompetidores'];
    		$d->imagen = $deporte['imagen'];
    		$d->save();
    	}
    }


    public function seedCompetidores()
    {
    	$c = new Competidor();
    	$c->nombre = "Andrés Trozado";
    	$c->pais = "España";
    	$c->fechaNacimiento = "2000-01-01";
    	$c->imagen = "competidor-11.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Atletismo%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Enrique Cido";
    	$c->pais = "Italia";
    	$c->fechaNacimiento = "1990-07-10";
    	$c->imagen = "competidor-12.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Natación%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Lola Mento";
    	$c->pais = "EEUU";
    	$c->fechaNacimiento = "2005-03-11";
    	$c->imagen = "competidor-02.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Badminton%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Alberto Mate";
    	$c->pais = "España";
    	$c->fechaNacimiento = "1995-02-28";
    	$c->imagen = "competidor-09.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Boxeo%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Estela Gartija";
    	$c->pais = "España";
    	$c->fechaNacimiento = "1999-04-19";
    	$c->imagen = "competidor-01.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Atletismo%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Elsa Capunta";
    	$c->pais = "España";
    	$c->fechaNacimiento = "1985-03-10";
    	$c->imagen = "competidor-04.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Triatlón%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Paca Garte";
    	$c->pais = "EEUU";
    	$c->fechaNacimiento = "1990-12-25";
    	$c->imagen = "competidor-03.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Natación%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Rubén Fermizo";
    	$c->pais = "Italia";
    	$c->fechaNacimiento = "1994-01-24";
    	$c->imagen = "competidor-07.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Natación%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Helen Chufe";
    	$c->pais = "EEUU";
    	$c->fechaNacimiento = "1987-06-03";
    	$c->imagen = "competidor-06.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Natación%")->first()->id;
    	$c->save();


    	$c = new Competidor();
    	$c->nombre = "Susana Torio";
    	$c->pais = "España";
    	$c->fechaNacimiento = "1984-05-29";
    	$c->imagen = "competidor-05.png";
    	$c->deporte_id = Deporte::where('nombre','like',"%Natación%")->first()->id;
    	$c->save();
    }
}
