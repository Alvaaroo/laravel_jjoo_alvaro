<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetidoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competidores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 120);  
            $table->string('pais');         
            $table->string('fechaNacimiento');    
            $table->string('imagen')->nullable();
            $table->string('numeroCompetidores')->default(-1);
            $table->unsignedbigInteger('deporte_id');
            $table->foreign('deporte_id')->references('id')->on('Deportes'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competidores');
    }
}
