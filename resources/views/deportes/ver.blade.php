@extends('layouts.master')
@section('titulo')
JJOO Tokyo (Mostrar)
@endsection
@section('contenido')
<div class="row">	
	<div class="col-md-12">

		<h2 style="min-height:45px;margin:5px 0 10px 0; color:#2ECC71;">{{$Deporte->nombre}}</h2>
		<h3 style="min-height:45px;margin:5px 0 10px 0;">Numero de pruebas: {{$Deporte->pruebas}}</h3>

		<h3 style="min-height:45px;margin:5px 0 10px 0;">Competidores</h3>
		<div class="row">
		@foreach($Deporte->competidores as $competidor)
			<div class="col-xs-12 col-sm-1 col-md-3">
	       	 	<h5 style="min-height:45px;margin:5px 0 10px 0; color:#7FB3D5;">{{$competidor->nombre}}</h5>
	      	  <img src="{{asset('assets/imagenes/competidores')}}/{{$competidor->imagen}}" alt="{{$competidor->nombre}}" class="img-fluid">
    	  </div>
		@endforeach
	</div>

	<div class="row" style="margin-top: 20px;">
	<div class="col-md-12">
		<a class="btn btn-primary" href="{{-- {{ url('/deportes/asignarnumeros/' . $Deporte->slug)}} --}}">Asignar numeros</a>
		<a class="btn btn-danger" href="{{-- {{ action('/deportes/resetearnumeros/' . $Deporte->slug)}} --}}">Resetear números</a>		
	</div>	
	</div>
	@if (count($Competidores) > 0)
	<table class="table" style="margin-top:30px;">
    <thead>
      <tr class="table-primary">
        <th>Nombre</th>
        <th>País</th>
        <th>Número asignado</th>
      </tr>
    </thead>
    <tbody> 
      @foreach($Competidores as $Competidor)
      	<tr>
      		<td>{{$Competidor->nombre}}</td>
      		<td>{{$Competidor->pais}}</td>
      		<td>{{$Competidor->numeroCompetidores}}</td>
      	</tr>
      @endforeach
    </tbody>
  </table>
  @endif
</div>
 {{-- @endforeach --}}
@endsection