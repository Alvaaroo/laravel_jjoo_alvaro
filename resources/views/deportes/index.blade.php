@extends('layouts.master')
@section('titulo')
JJOO Tokyo (index)
@endsection
@section('contenido')
 <div class="row">
    @foreach($deportes as $clave => $deporte)
    <div style="border:1px solid black;" class="col-xs-12 col-sm-1 col-md-4">
      <img src="{{asset('assets/imagenes/deportes')}}/{{$deporte->imagen}}" class='fluid' class="card-img-bottom" height="180" width="180" margin='10' />
        <a href="{{ url('/deportes/ver/' . $deporte->slug ) }}">
          <h4 style="min-height:45px;margin:5px 0 10px 0">
            {{$deporte->nombre}}
          </h4>
        </a>   
        @if (count($deporte->Competidores) == 1)
   
          <p>{{count($deporte->Competidores)}} Competidor</p>    
        @else
          <p>{{count($deporte->Competidores)}} Competidores</p> 
        @endif
    </div>
    @endforeach
  </div> 
@endsection