@extends('layouts.master')
@section('titulo')
JJOO Tokyo (incribir competidor)
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Añadir nuevo Competidor
			</div>
			<div class="card-body" style="padding:30px">
				{{-- Action del formulario con método POST --}}
				<form action="{{action('CompetidorController@postInscribir')}}" method="POST" enctype="multipart/form-data">	
					{{-- Cifrado del formulario --}}
					{{ csrf_field() }}
				<div class="form-group">
						
						<label for="nombre">Nombre</label>
						<input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre')}}">
				</div>

				<div class="form-group">
						
						<label for="pais">Pais</label>
						<input type="text" name="pais" id="pais" class="form-control" value="{{old('pais')}}">
				</div>

				<div class="form-group">
						
						<label for="fechaNacimiento">Fecha nacimiento</label>
						<input type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control" value="{{old('fechaNacimiento')}}">
				</div>
						
					<div class="form-group">
						<label for="deportes">Deportes</label>						
						<select type="text" name="deportes" id="deportes" class="form-control">
							@foreach($deportes as $deporte)

								<option value='{{$deporte->id}}'>{{$deporte->nombre}}</option>
							@endforeach
						</select>											
					</div>						
					<div class="form-group">
						{{-- TODO: Completa el input para la imagen --}}
						<label for="imagen">Imagen</label>
						<input type="file" class="form-control-file" id="imagen" name="imagen" value="{{old('imagen')}}">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
							Inscribir participante
						</button>
					</div>					
				</form>
				<script>
			      $(document).ready(function() {
			        $('#pais').autocomplete({

			          source: 

			          function (query, result) 
			          {
			            $.ajax({

				            type: "POST",

				            url: "{{url('busquedaAjax')}}",

				            dataType: "json",

				            data: {"_token": "{{ csrf_token() }}", "busqueda": query['term']},

				            success : function(data)
				            {
				             result(data);
				            }
			           	});
			          },

			          position: {

			                  my: "left+0 top+8", //Para mover la caja 8px abajo

			                }
			              });
			      });

			    </script>
			</div>
		</div>
	</div>
</div>
@endsection