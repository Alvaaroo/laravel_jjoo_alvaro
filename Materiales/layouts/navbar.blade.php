<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-success">
  <a class="navbar-brand" href="{{url('/')}}">JJOO Tokio 2020</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a href="{{url('/deportes')}}" class="nav-link {{ Request::is('deportes*') && !Request::is('competidores/inscribir')? ' active' : ''}}">Deportes</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/competidores/inscribir')}}" class="nav-link {{ Request::is('competidores/inscribir')? ' active' : ''}}">Inscribir competidor</a>
      </li>
    </ul>
    
  </div>
</nav>





