<?php 

//PON AQUÍ LA DIRECCIÓN DE TU WSDL
//$wsdl = "http://127.0.0.1/2019_2020-DWES/laravel_jjoo/public/api/wsdl";

$cliente = new SoapClient($wsdl);
$edad = 30;
$pais = "España";

?>



<!DOCTYPE html>
<html>
<head>
	<title>Cliente SOAP</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h2 class="text-primary">Competidores mayores que  <?php echo $edad ?> años</h2>
		<?php echo $cliente->getNumeroCompetidoresMayorEdad($edad); ?>

		<h2 class="mt-4 text-danger">Listado de competidores de <?php echo $pais; ?></h2>
		<ul>
		<?php 
			$competidores = $cliente->getParticipantesPorPais($pais);
			foreach ($competidores as $competidor) 
			{
				echo "<li>{$competidor->nombre}</li>";
			}
		?>
		</ul>

		
	</div>
</body>
</html>