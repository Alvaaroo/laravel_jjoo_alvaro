<?php

use Illuminate\Support\Facades\Route;
use App\Competidor;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){

	return redirect("deportes");
});

Route::get('deportes', 'DeporteController@getTodos');

Route::get('deportes/ver/{slug}', 'DeporteController@getVer');

Route::get('competidores/inscribir', 'CompetidorController@getInscribir');

Route::post('competidores/inscribir', 'CompetidorController@postInscribir');

Route::post("busquedaAjax", 'CompetidorController@Buscar');

Route::any('api', 'SoapServerController@getServer');

// Ruta que muestra el xml del wsdl
Route::any('api/wsdl', 'SoapServerController@getWSDL');

// Route::get('prueba', function(){

//         return Competidor::where("pais", "=", "España")->orderBy('fechaNacimiento')->get(); 
// });