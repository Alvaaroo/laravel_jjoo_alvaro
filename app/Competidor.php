<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
* @property string $nombre
* @property string $pais
* @property string $fechaNacimiento
* @property string $imagen
* @property int $numeroCompetidores
* @property string $deporte_id 
*/
class Competidor extends Model
{
    //

    protected $table = 'competidores';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function getEdad()
	{
		$fechaFormateada = Carbon::parse($this->fechaNacimiento);
		return $fechaFormateada->diffInYears(Carbon::now());
	}
}
