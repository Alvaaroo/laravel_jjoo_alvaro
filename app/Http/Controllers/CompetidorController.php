<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deporte;
use App\Competidor;

class CompetidorController extends Controller
{
    //

    public function getInscribir()
	{
		$deportes = Deporte::all();
		return view('competidores.inscribir', array("deportes" => $deportes));
	}	

	// Funcion donde se crea el objeto obra, se le pasan los parametros del formulario y si sale mal se redirige al formulario y si la crea correctamente se redirige a la pagina principal. enviando un mensaje.
	public function postInscribir(Request $Request)
	{
		$Competidor = new Competidor();

		if ($Request->has("nombre") && $Request->has("pais") && $Request->has('fechaNacimiento') && $Request->has("deportes") && $Request->has("imagen")){	
			
			$Competidor->nombre = $Request->nombre;
			$Competidor->pais = $Request->pais;
			$Competidor->fechaNacimiento = $Request->fechaNacimiento;
			$Competidor->imagen = $Request->imagen->store('', 'competidores');	
			$Competidor->deporte_id = $Request->deportes;		

		}else{

			return redirect('competidores/inscribir')->withInput();
		}
		
		$Competidor->save();
						   // Con el with se guardan variables en session
		return redirect('deportes')->with('mensaje', "Competidor: $Competidor->nombre inscrito correctamente");
	}

	 public function Buscar(Request $request)
	{
		$busqueda = $request->busqueda;
		$paises = Competidor::select("pais")->where('pais', 'like', "%".$busqueda."%")->pluck('pais')->unique();

		return response()->json($paises);
	}


}
