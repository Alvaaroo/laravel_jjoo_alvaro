<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deporte;
use App\Competidor;
use SoapServer;
use App\lib\WSDLDocument;

class SoapServerController extends Controller
{
    // Creacion de todas las variables necesarias para crear un servidor Soap.
    private $clase = "\\App\\Http\\Controllers\\JJOOWebService";
    private $uri = "http://localhost/EsteAnio/DWES/laravel_jjoo_Alvaro/public/api";
    private $servidor = "http://localhost/EsteAnio/DWES/laravel_jjoo_Alvaro/public/api";
    private $UrlWSDL = "http://localhost/EsteAnio/DWES/laravel_jjoo_Alvaro/public/api/wsdl";

    // Funcioón para crear el servidor.
    public function getServer()
    {
        $server = new SoapServer($this->UrlWSDL);
        $server->setClass($this->clase);
        $server->handle();
        exit();
    }

    // Función para crear y generar el WSDL.
    public function getWSDL()
    {
        $wsdl = new WSDLDocument($this->clase, $this->servidor, $this->uri);
        $wsdl->formatOutput = true;
        header('Content-Type: text/xml');
        echo $wsdl->saveXML();
        exit();
    }
}

class JJOOWebService
{
    /**
     * Funcion que devuelve el numero de competidores que tengan igual o más de una edad pasada como parámetro
     * @param int $edad 
     * @return int
     */    
    public function getNumeroCompetidoresMayorEdad($edad)
    {
        $competidores = Competidor::all();         
        $count = 0;
        foreach ($competidores as $competidor) {
         	
         	if ($competidor->getEdad() >= $edad) {
         		
         		$count++;
         	}
         } 

        return $count;  

    }

    /**
     * Funcion que nos devuelve un array de todas las obras
     * @param string $pais 
     * @return App\Competidor[]
     */
    
    public function getParticipantesPorPais($pais)
    {	
    	return Competidor::where("pais", "=", $pais)->orderBy('fechaNacimiento')->get();          
    }     
}