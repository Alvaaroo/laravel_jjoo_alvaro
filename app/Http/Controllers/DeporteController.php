<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deporte;
use App\Competidor;

class DeporteController extends Controller
{
    //

    public function getTodos()
    {
    	$deportes = Deporte::all();
    	return view('deportes.index', array("deportes" => $deportes));
    }

 	public function getVer($slug)
    {
    	$deporte = Deporte::where("slug", $slug)->first();
    	$competidores = Competidor::where("numeroCompetidores",'>', 0)->get();

    	return view('deportes.ver', array('Deporte' => $deporte, "Competidores" => $competidores));
    }   

    public function AsignarNumeros($slug)
    {

    }
}
