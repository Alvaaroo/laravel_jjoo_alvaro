<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* @property string $nombre
* @property string $slug
* @property string $pruebas
* @property string $numeroInicioCompetidores
* @property string $imagen
*/
class Deporte extends Model
{
    //

    protected $table = 'deportes';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function Competidores()
	{
		return $this->hasMany("App\Competidor");
	}
}
